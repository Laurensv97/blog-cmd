---
title: Donderdag 21 Juni
date: 2018-06-21
---

Vandaag heb ik mijn onderzoeksposter gepresenteerd. Ik heb feedback gekregen die grotendeels positief was op het uiterlijk van de poster. Ook heb ik deelgenomen aan een creatieve sessie. Hiervoor hebben we meerdere creatieve technieken toegepast eom zo tot een concept te komen. Dit heeft mij erg geholpen om mijn concept concreet te krijgen. Hierna hebben we gereflecteerd op het samenwerken tijdens deze creatieve sessie. De klasgenoten waar ik het mee deed vonden dat ik goede ideeen had en ook actief en gezellig meedeed met het toepassen van de creatieve technieken. Het concept heb ik toen verwerkt in een conceptposter die ik vandaag ook af heb gemaakt. Hierdoor heb ik wat tijd over zodat ik vooruit kan werken zodat ik een buffer creeer voor volgende week aangezien er die week veel werk verricht moet worden.