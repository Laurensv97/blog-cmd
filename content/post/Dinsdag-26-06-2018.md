---
title: Dinsdag 26 Juni
date: 2018-06-26
---

Vandaag ben ik begonnen met het testen van mijn paper prototype. Ik ben om half 9 naar Rotterdam gegaan om bij verschillende ov haltes mensen te vragen wat ze er van vonden. 
Ik heb hier meerdere resultaten uitgehaald en heb deze op school in een testrapportage gezet en de resultaten vervolgens geanalyseerd en hier een conclusie uit getrokken.
Na de pauze ben ik begonnen met het maken van een merkanalyse en een styleguide. Ik had al een styleguide gemaakt in OP2 die voldoende was gevalideerd dus heb
ik deze aangepast voor het huidige project.