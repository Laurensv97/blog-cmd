---
title: Donderdag 28 Juni
date: 2018-06-28
---

Vandaag was de laatste dag van de lange herkansingsroute. Ik heb vandaag mijn ontwerpproceskaart en mijn prototype afgemaakt. Gezien ik alle deliverables af heb kan ik mijn leerdossier gaan opstellen en mijn STARRTs gaan schrijven. Ik heb na deze twee weken eigenlijk pas echt goed doorgehad wat het allemaal inhoud om een echte CMD'er te zijn. En ik ben van mening dat deze twee weken mij als CMD'er een stuk beter hebben gemaakt op het gebied van het project doen zoals het hoort in plaats van hersenloos deliverables maken. Ondanks dat het een herkansingsroute was ben ik blij dat ik diot gedaan heb omdat het me heel erg heeft duidelijkgemaakt wat we allemaal dit jaar gedaan hebben en wat het nou precies is waar we naartoe werken als CMD studenten.