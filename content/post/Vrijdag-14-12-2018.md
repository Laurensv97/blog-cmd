---
title: Vrijdag 14 December
date: 2018-12-14
---

Deze week hebben we de conceptrichting aangepast zodat we meer voldoen aan de behoeften van Woonstad. Dit hebben we gedaan door een digitaal medium te bedenken dat ervoor zou moeten zorgen dat de wachtruimte bij de balie een betere sfeer krijgt. Dit hebben we maandag en donderdag uitgewerkt. Vrijdag kon ik in verband met een tandartsafspraak er helaas niet bijzijn. Wel heeft mijn team die dag het concept verder uitgewerkt.