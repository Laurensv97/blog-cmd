---
title: Donderdag 22 Juni
date: 2018-06-22
---

Vandaag ben ik begonnen met presenteren van mijn conceptposter. De feedback die ik hierop kreeg van mijn klasgenoten en van een docent heb ik opgeschreven en vervolgens heb ik de poster aangepast zodat deze voldoet aan de feedback. Hierna ben ik een concepttest gaan opstellen zodat ik binnenkort mijn concept kan gaan testen. Uiteindelijk heb ik nog een design rationale gemaakt en ben ik begonnen aan het maken van een user journey. 