---
title: Vrijdag 15 Maart
date: 2019-03-15
---

Deze week hebben we ons vooral beziggehouden met de beroepsproducten die we op de onderzoeksposter willen laten zien. 
Zo hebben we de taakverdeling uitgewerkt naar een planning, heb ik een researchplan gemaakt en heb ik deze ook uitgevoerd doormiddel van een ladering interview. Ik heb dit bij vier verschillende personen gedaan. 
De rest van het team zou dit ook gaan doen zodat we uiteindelijk genoeg resultaten hebben om te combineren met de deskresearch uitkomsten om zo een persona te maken.
 Ik heb ook verschilende kranten met elkaar vergeleken door te kijken naar het concurentieonderzoek om zo erachter te komen hoe het NRC zich onderscheid van de andere kranten. 
Ik ben op vrijdag bezig geweest met de interviews uittypen en het maken van een empathy map.