---
title: Maandag 4 September
date: 2017-09-04
---

Vandaag was de start van de **Design Challenge**. Samen met het team hebben we de taken verdeeld en een planning gemaakt voor de eerste iteratie. Zelf heb ik me verdiept in het thema van de game die we moesten maken. Na veel nadenken had ik het idee om een game te maken wat in principe een speurtocht is door Rotterdam die gebruik maakt van het scannen van een QR-code. Uit onderzoek met de gekozen doelgroep bleek dit een goed idee aangezien de doelgroep het leuk vond om een tour door Rotterdam te doen waarbij ze allemaal kunstzinnige plekken bezochten. De QR-code scannen bleek uiteindelijk niet heel interactief te zijn. Vervolgens werd dat idee omgezet in een quiz die gebruik maakt van vragen op basis van de locatie waar de gebruiker zich bevind. Hier heb ik ook een wireframe voor gemaakt. 